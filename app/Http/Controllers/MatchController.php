<?php

namespace App\Http\Controllers;

use App\Feature;
use Illuminate\Contracts\Support\Renderable;

class MatchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Renderable
     */
    public function index()
    {
        $features = Feature::whereIn('id', request('id', []))->get();

        return view('match.index', compact('features'));
    }
}
