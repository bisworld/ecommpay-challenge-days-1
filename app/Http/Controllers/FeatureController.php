<?php

namespace App\Http\Controllers;

use App\Feature;
use App\Models\FeatureFeature;
use App\Models\FeatureInterface;
use App\Models\FeatureOperation;
use App\Models\Features;
use App\Models\Interfaces;
use App\Models\Operations;
use Illuminate\Http\RedirectResponse;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Contracts\Support\Renderable;
use App\Http\Requests\Features\StoreRequest;
use App\Http\Requests\Features\UpdateRequest;

class FeatureController extends Controller
{
    /**
     * FeatureController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth')->only(['store', 'edit', 'update', 'destroy']);
    }

    const FEATURES_STATUS = ['forbidden', 'allowable', 'going', 'necessary'];

    /**
     * Display a listing of the resource.
     *
     * @return Renderable
     */
    public function index()
    {
        $feature = new Feature();

        if (request('business_type')) {
            $feature = $feature->whereHas('merchants', function (Builder $query) {
                $query->where('business_type', request('business_type'));
            });
        }

        if (request('operation_id')) {
            $feature = $feature->whereHas('operations', function (Builder $query) {
                $query->where('id', request('operation_id'));
            });
        }

        if (request('interface_id')) {
            $feature = $feature->whereHas('interfaces', function (Builder $query) {
                $query->where('id', request('interface_id'));
            });
        }

        $features = $feature->get();

        return view('features.index', compact('features'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Renderable
     */
    public function create()
    {
        $operations = Operations::all();
        $interfaces = Interfaces::all();
        $features = Features::all();
        $featuresStatus = self::FEATURES_STATUS;

        return view('features.create', compact('operations', 'interfaces', 'features', 'featuresStatus'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreRequest $request
     * @return RedirectResponse
     */
    public function store(StoreRequest $request)
    {
        $data = $request->all();

        $featureData = [
            'name' => $data['name'],
            'description' => $data['description'],
            'link_conf' => $data['link_conf'],
            'link_jira' => $data['link_jira']
        ];

        $feature = new Features($featureData);
        $feature->save();

        if (!empty($data['operations'])) {
            foreach ($data['operations'] as $operation) {
                FeatureOperation::insert(['feature_id' => $feature->id, 'operation_id' => $operation]);
            }
        }
        if (!empty($data['interfaces'])) {
            foreach ($data['interfaces'] as $interfaces) {
                FeatureInterface::insert(['feature_id' => $feature->id, 'interface_id' => $interfaces]);
            }
        }

        if (!empty($data['status'])) {
            $insert = [];
            foreach ($data['status'] as $status) {
                $ffeatures = explode("_", $status);
                $insert[] = [
                    'feature1_id' => $feature->id,
                    'feature2_id' => $ffeatures[0],
                    'relation' => $ffeatures[1]
                ];
            }
            FeatureFeature::insert($insert);
        }

        return redirect()->route('features.index')->with('message', 'Фича создана!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Renderable
     */
    public function show($id)
    {
        $item = Features::find($id);

        $operations = Operations::join('feature_operation', 'feature_operation.operation_id', '=', 'operations.id')
            ->select('operations.name')
            ->where('feature_operation.feature_id', $id)
            ->get();

        $interfaces = Interfaces::join('feature_interface', 'feature_interface.interface_id', '=', 'interfaces.id')
            ->select('interfaces.name')
            ->where('feature_interface.feature_id', $id)
            ->get();

        $features = Features::where('id', '<>', $id)->get();
        $result = FeatureFeature::where('feature1_id', $id)->orWhere('feature2_id', $id)->get()->toArray();
        $ffeaturesArr = [];
        foreach ($features as $feature) {
            foreach ($result as $val) {
                $featureId = $val['feature1_id'] == $feature->id ? $val['feature1_id'] : $val['feature2_id'];
                if ($val['feature1_id'] == $feature->id || $val['feature2_id'] == $feature->id) {
                    $ffeaturesArr[] = $featureId;
                }
            }
        }
        $ffeaturesArr = array_unique($ffeaturesArr);
        foreach ($features as $feature) {
            $status = 'forbidden';
            if (in_array($feature->id, $ffeaturesArr, true)) {
                foreach ($result as $val) {
                    if ($val['feature1_id'] == $feature->id || $val['feature2_id'] == $feature->id) {
                        $status = $val['relation'];
                    }
                }
            }

            $ffeatures[] = [
                'id' => $feature->id,
                'name' => $feature->name,
                'status' => $status
            ];
        }


        return view('features.show', compact('item', 'operations', 'interfaces', 'ffeatures'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Renderable
     */
    public function edit($id)
    {
        $operations = Operations::all();
        $interfaces = Interfaces::all();
        $features = Features::where('id', '<>', $id)->get();

        $item = Features::find($id);
        $result = FeatureOperation::select('operation_id')->where('feature_id', $id)->get()->toArray();
        foreach ($result as $val) {
            $foperations[] = $val['operation_id'];
        }

        $result = "";
        $result = FeatureInterface::select('interface_id')->where('feature_id', $id)->get()->toArray();
        foreach ($result as $val) {
            $finterfaces[] = $val['interface_id'];
        }

        $result = "";
        $result = FeatureFeature::where('feature1_id', $id)->orWhere('feature2_id', $id)->get()->toArray();
        $ffeaturesArr = [];
        foreach ($features as $feature) {
            foreach ($result as $val) {
                $featureId = $val['feature1_id'] == $feature->id ? $val['feature1_id'] : $val['feature2_id'];
                if ($val['feature1_id'] == $feature->id || $val['feature2_id'] == $feature->id) {
                    $ffeaturesArr[] = $featureId;
                }
            }
        }
        $ffeaturesArr = array_unique($ffeaturesArr);
        foreach ($features as $feature) {
            $status = 'forbidden';
            if (in_array($feature->id, $ffeaturesArr, true)) {
                foreach ($result as $val) {
                    if ($val['feature1_id'] == $feature->id || $val['feature2_id'] == $feature->id) {
                        $status = $val['relation'];
                    }
                }
            }

            $ffeatures[] = [
                    'id' => $feature->id,
                    'name' => $feature->name,
                    'status' => $status
            ];
        }
        $featuresStatus = self::FEATURES_STATUS;

        return view('features.edit', compact(
            'operations',
            'interfaces',
            'features',
            'item',
            'foperations',
            'finterfaces',
            'ffeatures',
            'featuresStatus'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateRequest $request
     * @param  int  $id
     * @return RedirectResponse
     */
    public function update(UpdateRequest $request, $id)
    {
        $data = $request->all();

        $featureData = [
            'name' => $data['name'],
            'description' => $data['description'],
            'link_conf' => $data['link_conf'],
            'link_jira' => $data['link_jira']
        ];

        $feature = Features::find($id);
        $affected = Features::where('id', $id)
            ->update($featureData);

        if (!empty($data['operations'])) {
            FeatureOperation::whereIn('feature_id', $data['operations'])->delete();

            foreach ($data['operations'] as $operation) {
                FeatureOperation::insert(['feature_id' => $feature->id, 'operation_id' => $operation]);
            }
        }
        if (!empty($data['interfaces'])) {
            FeatureInterface::whereIn('feature_id', $data['operations'])->delete();

            foreach ($data['interfaces'] as $interfaces) {
                FeatureInterface::insert(['feature_id' => $feature->id, 'interface_id' => $interfaces]);
            }
        }

        if (!empty($data['status'])) {
            $insert = [];
            foreach ($data['status'] as $status) {
                $ffeatures = explode("_", $status);
                FeatureFeature::where('feature1_id', $feature->id)->orWhere('feature2_id', $feature->id)->delete();
                $insert[] = [
                    'feature1_id' => $feature->id,
                    'feature2_id' => $ffeatures[0],
                    'relation' => $ffeatures[1]
                ];
            }
            FeatureFeature::insert($insert);
        }

        return redirect()->route('features.index')->with('message', 'Фича обновлена!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return RedirectResponse
     */
    public function destroy($id)
    {
        Features::where('id', $id)->delete();

        return redirect()->route('features.index')->with('message', 'Фича удалена!');
    }
}
