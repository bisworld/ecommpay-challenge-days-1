<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Feature extends Model
{
    /**
     * @inheritDoc
     */
    protected $fillable = [
        'id', 'name', 'description', 'link_conf', 'link_jira'
    ];

    /**
     * @return BelongsToMany
     */
    public function merchants()
    {
        return $this->belongsToMany(Merchant::class);
    }

    /**
     * @return BelongsToMany
     */
    public function operations()
    {
        return $this->belongsToMany(Operation::class);
    }

    /**
     * @return BelongsToMany
     */
    public function interfaces()
    {
        return $this->belongsToMany(InterfaceType::class, 'feature_interface', null, 'interface_id');
    }

    /**
     * @return BelongsToMany|Feature
     */
    public function features()
    {
        return $this->belongsToMany(Feature::class, null, 'feature1_id', 'feature2_id')->withPivot('relation');
    }
}
