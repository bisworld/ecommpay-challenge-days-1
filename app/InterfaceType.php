<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InterfaceType extends Model
{
    /**
     * @inheritDoc
     */
    protected $table = 'interfaces';
}
