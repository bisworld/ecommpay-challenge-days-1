<?php

namespace App\Services;

use App\Operation;
use Illuminate\Support\Collection;

class OperationService
{
    /**
     * @var Operation
     */
    private $operation;

    /**
     * OperationService constructor.
     *
     * @param Operation $operation
     */
    public function __construct(Operation $operation)
    {
        $this->operation = $operation;
    }

    /**
     * @return Collection
     */
    public function list()
    {
        return $this->operation->get()->pluck('name', 'id');
    }
}
