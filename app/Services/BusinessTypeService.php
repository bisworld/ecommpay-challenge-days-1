<?php

namespace App\Services;

use App\BusinessType;
use Illuminate\Support\Collection;

class BusinessTypeService
{
    /**
     * @var BusinessType
     */
    private $businessType;

    /**
     * BusinessTypeService constructor.
     *
     * @param BusinessType $businessType
     */
    public function __construct(BusinessType $businessType)
    {
        $this->businessType = $businessType;
    }

    /**
     * @return Collection
     */
    public function list()
    {
        return $this->businessType->get()->pluck('name', 'id');
    }
}
