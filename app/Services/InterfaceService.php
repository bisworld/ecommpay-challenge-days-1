<?php

namespace App\Services;

use App\InterfaceType;
use Illuminate\Support\Collection;

class InterfaceService
{
    /**
     * @var InterfaceType
     */
    private $interfaceType;

    /**
     * InterfaceService constructor.
     *
     * @param InterfaceType $interfaceType
     */
    public function __construct(InterfaceType $interfaceType)
    {
        $this->interfaceType = $interfaceType;
    }

    /**
     * @return Collection
     */
    public function list()
    {
        return $this->interfaceType->get()->pluck('name', 'id');
    }
}
