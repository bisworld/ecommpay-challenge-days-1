<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FeatureMerchant extends Model
{
    public $timestamps = false;
    protected $table = 'feature_merchant';
    protected $primaryKey = 'id';
    protected $fillable =[
        'feature_id',
        'merchant_id'
    ];
}
