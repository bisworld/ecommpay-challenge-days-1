<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BysinessType extends Model
{
    public $timestamps = false;
    protected $table = 'bysiness_types';
    protected $primaryKey = 'id';
    protected $fillable =[
        'id',
        'name'
    ];
}
