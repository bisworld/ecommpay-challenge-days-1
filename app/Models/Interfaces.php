<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Interfaces extends Model
{
    public $timestamps = false;
    protected $table = 'interfaces';
    protected $primaryKey = 'id';
    protected $fillable =[
        'id',
        'name'
    ];
}
