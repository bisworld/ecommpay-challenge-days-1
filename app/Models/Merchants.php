<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Merchants extends Model
{
    public $timestamps = false;
    protected $table = 'merchants';
    protected $primaryKey = 'id';
    protected $fillable =[
        'id',
        'name',
        'business_type'
    ];
}
