<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FeatureFeature extends Model
{
    public $timestamps = false;
    protected $table = 'feature_feature';
    protected $fillable =[
        'feature1_id',
        'feature2_id',
        'relation'
    ];
}
