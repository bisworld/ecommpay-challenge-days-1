<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Operations extends Model
{
    public $timestamps = false;
    protected $table = 'operations';
    protected $primaryKey = 'id';
    protected $fillable =[
        'id',
        'name'
    ];
}
