<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FeatureOperation extends Model
{
    public $timestamps = false;
    protected $table = 'feature_operation';
    protected $fillable =[
        'feature_id',
        'operation_id'
    ];
}
