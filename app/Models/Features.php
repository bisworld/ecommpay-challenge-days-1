<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Features extends Model
{
    public $timestamps = false;
    protected $table = 'features';
    protected $primaryKey = 'id';
    protected $fillable =[
        'id',
        'name',
        'description',
        'link_conf',
        'link_jira'
    ];
}
