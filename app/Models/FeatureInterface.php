<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FeatureInterface extends Model
{
    public $timestamps = false;
    protected $table = 'feature_interface';
    protected $fillable =[
        'feature_id',
        'interface_id'
    ];
}
