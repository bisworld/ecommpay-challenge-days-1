@extends('layouts.app')

@section('content')
    <div class="container">

        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">

                        <div class="form-group">
                            <div class="row form-group">
                                <div class="col">
                                    <h4>Title:</h4>
                                    <div>{{$item->name}}</div>
                                </div>
                            </div>
                            <div class="form-group">
                                <h4 for="description">Description:</h4>
                                <div>{{$item->description}}</div>
                            </div>

                            <h4 class="mb">Usage for operations</h4>
                            @foreach($operations as $operation)
                            <div class="row form-group">
                                <div class="col">
                                    <div>{{$operation->name}}</div>
                                </div>
                            </div>
                            @endforeach

                            <h4 class="mb">Usage with interface</h4>
                            @foreach($interfaces as $interface)
                            <div class="row form-group">
                                <div class="col">
                                    <div>{{$interface->name}}</div>
                                </div>
                            </div>
                            @endforeach

                            <h4 class="mb">Usage for feature</h4>
                            @foreach($ffeatures as $feature)
                                <div class="row form-group">
                                    <div class="col form-inline">
                                        <div class="col-md-2">{{$feature['name']}}</div>
                                        <div class="col-md-2">{{$feature['status']}}</div>
                                    </div>
                                </div>
                            @endforeach

                            <div class="row form-group">
                                <div class="col">
                                    <h4>Link to confluence:</h4>
                                    <div><a href="{{$item->link_conf}}" target="_blank">{{$item->link_conf}}</a></div>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col">
                                    <h4>Link to JIRA:</h4>
                                    <div><a href="{{$item->link_jira}}" target="_blank">{{$item->link_jira}}</a></div>
                                </div>
                            </div>

                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-8">
                                <a class="btn btn-primary" href="{{ route('features.index') }}">Back</a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
