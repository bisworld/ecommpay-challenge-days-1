@extends('layouts.app')

@section('content')
    <div class="container">

        <div class="row justify-content-center">
            <div class="col-md-12">

                <div class="card">
                    <div class="card-body">
                        @if($errors->any())
                            <div class="row justify-content-center">
                                <div class="col-md-12">
                                    <div class="alert alert-danger" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">x</span>
                                        </button>
                                        {{ $errors->first() }}
                                    </div>
                                </div>
                            </div>
                        @endif

                        @if(session('success'))
                            <div class="row justify-content-center">
                                <div class="col-md-12">
                                    <div class="alert alert-success" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">x</span>
                                        </button>
                                        {{ session()->get('success') }}
                                    </div>
                                </div>
                            </div>
                        @endif

                        <form method="POST" action="{{ route('features.store') }}">
                            @csrf
                            <div class="form-group">
                                <div class="row form-group">
                                    <div class="col">
                                        <h4>Title:</h4>
                                        <input type="text" class="form-control" name="name"
                                               value="{{ old('name') }}" placeholder="title">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <h4 for="description">Description:</h4>
                                    <textarea id="description" name="description" class="form-control"></textarea>
                                </div>
                                <div class="row form-group">
                                    <div class="col">
                                        <h4>Link to confluence:</h4>
                                        <input type="text" class="form-control" name="link_conf"
                                               value="{{ old('link_conf') }}" placeholder="Link to confluence">
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col">
                                        <h4>Link to JIRA:</h4>
                                        <input type="text" class="form-control" name="link_jira"
                                               value="{{ old('link_jira') }}" placeholder="Link to JIRA">
                                    </div>
                                </div>

                                <h4 class="mb">Usage for operations</h4>
                                @foreach($operations as $operation)
                                <div class="row form-group">
                                    <div class="col">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="operationCheck{{$operation->id}}" name="operations[]" value="{{$operation->id}}">
                                            <label class="custom-control-label" for="operationCheck{{$operation->id}}">{{$operation->name}}</label>
                                        </div>
                                    </div>
                                </div>
                                @endforeach

                                <h4 class="mb">Usage with interface</h4>
                                @foreach($interfaces as $interface)
                                    <div class="row form-group">
                                        <div class="col">
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="interfaceCheck{{$interface->id}}" name="interfaces[]" value="{{$interface->id}}">
                                                <label class="custom-control-label" for="interfaceCheck{{$interface->id}}">{{$interface->name}}</label>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach

                                <h4 class="mb">Usage for feature</h4>
                                @foreach($features as $feature)
                                    <div class="row form-group">
                                        <div class="col form-inline">
                                            <div class="col-md-2">{{$feature->name}}</div>
                                            <select class="custom-select mr-sm-2 col-md-2" name="status[]">
                                                @foreach($featuresStatus as $status)
                                                    <option value="{{$feature->id}}_{{$status}}">{{$status}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            <div class="form-group row mb-0">
                                <div class="col-md-8">
                                    <button type="submit" class="btn btn-primary">Save</button>

                                    <a class="btn btn-primary" href="{{ route('features.index') }}">Back</a>

                                </div>
                            </div>
                        </form>


                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
