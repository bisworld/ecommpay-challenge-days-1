@inject('businessType', "App\Services\BusinessTypeService")
@inject('operation', "App\Services\OperationService")
@inject('interfaceType', "App\Services\InterfaceService")
@extends('layouts.app')

@section('title', 'Features')

@section('content')
    <div class="container">
        <div class="justify-content-center">
            <h1>Features list</h1>

            {!! Form::open(['route' => 'features.index', 'method' => 'get', 'class' => 'row']) !!}
                <div class="col-sm my-2">
                    {!! Form::select('business_type', $businessType->list(), request('business_type'), [
                        'placeholder' => 'Choose business type...',
                        'class'       => 'custom-select mr-sm-2',
                    ]) !!}
                </div>

                <div class="col-sm my-2">
                    {!! Form::select('operation_id', $operation->list(), request('operation_id'), [
                        'placeholder' => 'Choose operation type...',
                        'class'       => 'custom-select mr-sm-2',
                    ]) !!}
                </div>

                <div class="col-sm my-2">
                    {!! Form::select('interface_id', $interfaceType->list(), request('interface_id'), [
                        'placeholder' => 'Choose interface...',
                        'class'       => 'custom-select mr-sm-2',
                    ]) !!}
                </div>

                <div class="col-sm my-2">
                    {!! Form::text('name', request('name'), [
                        'placeholder' => 'Enter name...',
                        'class'       => 'form-control mr-sm-2',
                    ]) !!}
                </div>

                <div class="col-sm my-2 text-right">
                    <button type="submit" class="btn btn-primary">Search</button>
                </div>
            {!! Form::close() !!}

            {!! Form::open(['route' => 'match', 'method' => 'get', 'class' => 'row']) !!}
            <table class="table table-hover">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name feature</th>
                    <th scope="col" class="text-right">Actions</th>
                </tr>
                </thead>
                <tbody>
                @forelse($features as $feature)
                    <tr>
                        <th scope="col">
                            {!! Form::checkbox('id[]', $feature->id) !!}
                        </th>
                        <td>{{ $feature->name }}</td>
                        <td class="text-right">
                            <a href="{{$feature->link_conf}}" class="btn bg-info btn-sm"
                               title="Link to confluence" target="_blank">
                                <i class="fa fa-info-circle"></i>
                            </a>
                            <a href="{{$feature->link_jira}}" class="btn bg-info btn-sm"
                               title="Link to JIRA" target="_blank">
                                <i class="fa fa-code"></i>
                            </a>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <a href="{!! route('features.show', [$feature->id]) !!}" class="btn btn-success btn-sm"
                               title="show">
                                <i class="far fa-eye"></i>
                            </a>
                            @auth
                            <a href="{!! route('features.edit', [$feature->id]) !!}" class="btn btn-primary btn-sm"
                               title="edit">
                                <i class="fas fa-pen"></i>
                            </a>
                            <a href="{!! route('destroy', [$feature->id]) !!}" class="btn btn-danger btn-sm"
                               title="delete">
                                <i class="fas fa-trash-alt"></i>
                            </a>
                            @endauth
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="3">
                            <div class="alert alert-warning mb-0" role="alert">
                                Features not found!
                            </div>
                        </td>
                    </tr>
                @endforelse
                </tbody>
            </table>

            <div class="container">
                <div class="row">
                    @if($features->count())
                    <div class="col-10">
                        <button type="submit" class="btn btn-primary">Match</button>
                    </div>
                    @endif

                    <div class="col-2">
                        @auth
                        <a class="btn btn-success" href="{{ route('features.create') }}">Add Feature</a>
                        @endauth
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
