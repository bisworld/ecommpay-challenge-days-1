@extends('layouts.app')

@section('title', 'Match')

@section('content')
    <div class="container">
        <div class="justify-content-center">
            <h1>Compatibility check</h1>
            @foreach($features as $feature)
                <table class="table table-hover mb-5">
                    <thead>
                    <tr>
                        <th scope="col" width="50%">&nbsp;</th>
                        <th scope="col">{{ $feature->name }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($feature->features as $item)
                        <tr>
                            <th>{{ $item->name }}</th>
                            <td><span class="badge text-{{ $item->pivot->relation }}">{{ $item->pivot->relation }}</span></td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="2">
                                <div class="alert alert-warning mb-0" role="alert">
                                    Features not found!
                                </div>
                            </td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
            @endforeach

            <a href="{{ route('features.index') }}" class="btn btn-success">Back</a>
        </div>
    </div>
@stop
