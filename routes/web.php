<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('features/{id}/destroy/', 'FeatureController@destroy')->name('destroy');
Route::resource('features', 'FeatureController', [
    'names' => [
        'index'   => 'features.index',
        'create'  => 'features.create',
        'store'   => 'features.store',
        'show'    => 'features.show',
        'edit'    => 'features.edit',
        'update'  => 'features.update'
    ]
]);

Auth::routes();

Route::get('/match', 'MatchController@index')->name('match');

Route::get('/home', 'HomeController@index')->name('home');
