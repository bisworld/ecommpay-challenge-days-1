<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFeatureFeatureTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('feature_feature');
        Schema::create('feature_feature', function (Blueprint $table) {
            $table->integer('feature1_id');
            $table->integer('feature2_id');
            $table->enum('relation', ['forbidden', 'allowable', 'going', 'necessary']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('feature_feature');
    }
}
