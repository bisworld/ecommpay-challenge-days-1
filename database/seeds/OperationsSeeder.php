<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OperationsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $datas = [
            [
                'id' => '1',
                'name' => 'sale',
            ],
            [
                'id' => '2',
                'name' => 'refund'
            ],
            [
                'id' => '3',
                'name' => 'payout'
            ]
        ];

        foreach ($datas as $data)
            DB::table('operations')->insert($data);
    }
}
