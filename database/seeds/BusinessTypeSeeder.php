<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BusinessTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $datas = [
            [
                'id' => '1',
                'name' => 'Travel',
            ],
            [
                'id' => '2',
                'name' => 'Hospitality'
            ],
            [
                'id' => '3',
                'name' => 'Digital Platforms'
            ],
            [
                'id' => '4',
                'name' => 'Online Gaming'
            ],
            [
                'id' => '5',
                'name' => 'iGaming and Trading'
            ],
            [
                'id' => '6',
                'name' => 'Financial Services'
            ],
            [
                'id' => '7',
                'name' => 'Professional Services'
            ],
        ];

        foreach ($datas as $data)
            DB::table('business_types')->insert($data);
    }
}
