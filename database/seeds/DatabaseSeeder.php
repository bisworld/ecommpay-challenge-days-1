<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(BusinessTypeSeeder::class);
        $this->call(FeaturesSeeder::class);
        $this->call(InterfacesSeeder::class);
        $this->call(OperationsSeeder::class);
        $this->call(MerchantsSeeder::class);
        $this->call(FeatureInterfaceSeeder::class);
        $this->call(FeatureOperationSeeder::class);
        $this->call(FeatureMerchantSeeder::class);
        $this->call(FeatureFeatureSeeder::class);
        $this->call(UserSeeder::class);
    }
}
