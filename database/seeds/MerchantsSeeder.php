<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MerchantsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $datas = [
            [
                'id' => '1',
                'name' => 'Travel Merchant',
                'business_type' => '1'
            ],
            [
                'id' => '2',
                'name' => 'Hospitality Merchant',
                'business_type' => '2'
            ],
            [
                'id' => '3',
                'name' => 'Digital Platforms Merchant',
                'business_type' => '3'
            ],
            [
                'id' => '4',
                'name' => 'Online Gaming Merchant',
                'business_type' => '4'
            ]
        ];

        foreach ($datas as $data)
            DB::table('merchants')->insert($data);
    }
}
