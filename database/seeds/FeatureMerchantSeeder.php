<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FeatureMerchantSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $datas = [
            [
                'feature_id'  => '1',
                'merchant_id' => '1'
            ],
            [
                'feature_id'  => '1',
                'merchant_id' => '2'
            ],
            [
                'feature_id'  => '1',
                'merchant_id' => '3'
            ],
            [
                'feature_id'  => '2',
                'merchant_id' => '1'
            ],
            [
                'feature_id'  => '2',
                'merchant_id' => '2'
            ],
            [
                'feature_id'  => '2',
                'merchant_id' => '4'
            ],
            [
                'feature_id'  => '3',
                'merchant_id' => '4'
            ],
            [
                'feature_id'  => '3',
                'merchant_id' => '4'
            ],
            [
                'feature_id'  => '4',
                'merchant_id' => '1'
            ],
            [
                'feature_id'  => '4',
                'merchant_id' => '3'
            ],
            [
                'feature_id'  => '4',
                'merchant_id' => '4'
            ],
            [
                'feature_id'  => '5',
                'merchant_id' => '2'
            ],
            [
                'feature_id'  => '4',
                'merchant_id' => '4'
            ],
            [
                'feature_id'  => '6',
                'merchant_id' => '1'
            ],
            [
                'feature_id'  => '6',
                'merchant_id' => '2'
            ],
            [
                'feature_id'  => '6',
                'merchant_id' => '3'
            ],
            [
                'feature_id'  => '6',
                'merchant_id' => '4'
            ]
        ];

        foreach ($datas as $data) {
            DB::table('feature_merchant')->insert($data);
        }
    }
}
