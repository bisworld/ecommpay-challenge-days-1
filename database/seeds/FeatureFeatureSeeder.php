<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FeatureFeatureSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $datas = [
            [
                'feature1_id' => '1',
                'feature2_id' => '1',
                'relation'    => 'forbidden'
            ],
            [
                'feature1_id' => '1',
                'feature2_id' => '2',
                'relation'    => 'going'
            ],
            [
                'feature1_id' => '1',
                'feature2_id' => '3',
                'relation'    => 'going'
            ],
            [
                'feature1_id' => '1',
                'feature2_id' => '4',
                'relation'    => 'going'
            ],
            [
                'feature1_id' => '1',
                'feature2_id' => '5',
                'relation'    => 'going'
            ],

            [
                'feature1_id' => '2',
                'feature2_id' => '1',
                'relation'    => 'going'
            ],
            [
                'feature1_id' => '2',
                'feature2_id' => '3',
                'relation'    => 'forbidden'
            ],
            [
                'feature1_id' => '2',
                'feature2_id' => '5',
                'relation'    => 'going'
            ],
            [
                'feature1_id' => '2',
                'feature2_id' => '6',
                'relation'    => 'going'
            ],
            [
                'feature1_id' => '3',
                'feature2_id' => '2',
                'relation'    => 'forbidden'
            ],
            [
                'feature1_id' => '3',
                'feature2_id' => '3',
                'relation'    => 'going'
            ],
            [
                'feature1_id' => '3',
                'feature2_id' => '4',
                'relation'    => 'going'
            ],
            [
                'feature1_id' => '3',
                'feature2_id' => '5',
                'relation'    => 'necessary'
            ],
            [
                'feature1_id' => '4',
                'feature2_id' => '1',
                'relation'    => 'going'
            ],
            [
                'feature1_id' => '4',
                'feature2_id' => '2',
                'relation'    => 'going'
            ],
            [
                'feature1_id' => '4',
                'feature2_id' => '3',
                'relation'    => 'going'
            ],
            [
                'feature1_id' => '4',
                'feature2_id' => '4',
                'relation'    => 'going'
            ],
            [
                'feature1_id' => '4',
                'feature2_id' => '5',
                'relation'    => 'going'
            ],
        ];

        foreach ($datas as $data) {
            DB::table('feature_feature')->insert($data);
        }
    }
}
