<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class InterfacesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $datas = [
            [
                'id' => '1',
                'name' => 'PP',
            ],
            [
                'id' => '2',
                'name' => 'Gate'
            ],
            [
                'id' => '3',
                'name' => 'SDK'
            ]
        ];

        foreach ($datas as $data)
            DB::table('interfaces')->insert($data);
    }
}
