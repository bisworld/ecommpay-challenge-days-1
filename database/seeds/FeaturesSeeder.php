<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FeaturesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $datas = [
            [
                'id' => '1',
                'name' => '3DS 2.0',
                'description' => '3DSecure, also known as payer authentication or payment authentication, is a security protocol initiated and created by Visa and Mastercard. 

3DSecure authenticates the cardholder during online payment processing by latter confirming transaction with the unique password. The first & foremost mission of 3DSecure protocol is to combat online payments fraud.',
                'link_conf' => 'https://confluence.ecommpay.com/display/ECP0LV0OCS/3DS+2.0',
                'link_jira' => 'https://jira.ecommpay.com/browse/PMO-1788'
            ],
            [
                'id' => '2',
                'name' => 'Cascading',
                'description' => 'Cascading reroutes merchant payments and payouts in cases of declined transactions, which increases the payment success rates.',
                'link_conf' => 'https://confluence.ecommpay.com/pages/viewpage.action?pageId=51153710',
                'link_jira' => 'https://jira.ecommpay.com/browse/CORE-5779'
            ],
            [
                'id' => '3',
                'name' => 'Routing',
                'description' => 'Smart Payment Routing is a flexible payment solution. Taking into consideration all key features and functionalities of banking products, Payment Routing enables high payment acceptance rates by easily connecting to the client’s infrastructure to optimise all incoming and outgoing transactions.',
                'link_conf' => 'https://confluence.ecommpay.com/pages/viewpage.action?pageId=77694060',
                'link_jira' => 'https://jira.ecommpay.com/browse/BOF-2841'
            ],
            [
                'id' => '4',
                'name' => 'P2P',
                'description' => 'Person-to-person payments (P2P) is an online technology that allows customers to transfer funds from their bank account or credit card to another individual\'s account via the Internet or a mobile phone.',
                'link_conf' => 'https://confluence.ecommpay.com/display/QA/P2P+Money+Transfer',
                'link_jira' => 'https://jira.ecommpay.com/browse/CORE-5977'
            ],
            [
                'id' => '5',
                'name' => 'Payment link',
                'description' => 'Create a link to a secure hosted payment page through which to request payments from your customers via e-mail or messenger in just a few clicks.',
                'link_conf' => 'https://confluence.ecommpay.com/pages/viewpage.action?pageId=69212024',
                'link_jira' => 'https://jira.ecommpay.com/browse/BI-1664'
            ],
            [
                'id' => '6',
                'name' => 'Try Again',
                'description' => 'To increase conversion rates, Payment Page supports processing of additional payment attempts when after a decline a customer can click Try Again button and return to the first step to select another payment account, or to just simply try again.',
                'link_conf' => 'https://confluence.ecommpay.com/display/QA/Try+again',
                'link_jira' => 'https://jira.ecommpay.com/browse/PP-4556'
            ]
        ];

        foreach ($datas as $data)
            DB::table('features')->insert($data);
    }
}
