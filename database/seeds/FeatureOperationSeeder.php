<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FeatureOperationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $datas = [
            [
                'feature_id'   => '1',
                'operation_id' => '1'
            ],
            [
                'feature_id'   => '2',
                'operation_id' => '1'
            ],
            [
                'feature_id'   => '2',
                'operation_id' => '3'
            ],
            [
                'feature_id'   => '3',
                'operation_id' => '1'
            ],
            [
                'feature_id'   => '4',
                'operation_id' => '1'
            ],
            [
                'feature_id'   => '4',
                'operation_id' => '3'
            ],
            [
                'feature_id'   => '5',
                'operation_id' => '1'
            ],
            [
                'feature_id'   => '5',
                'operation_id' => '2'
            ],
            [
                'feature_id'   => '6',
                'operation_id' => '1'
            ],
            [
                'feature_id'   => '6',
                'operation_id' => '3'
            ],
        ];

        foreach ($datas as $data) {
            DB::table('feature_operation')->insert($data);
        }
    }
}
