<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FeatureInterfaceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $datas = [
            [
                'feature_id'   => '1',
                'interface_id' => '1'
            ],
            [
                'feature_id'   => '1',
                'interface_id' => '2'
            ],
            [
                'feature_id'   => '1',
                'interface_id' => '3'
            ],
            [
                'feature_id'   => '2',
                'interface_id' => '1'
            ],
            [
                'feature_id'   => '2',
                'interface_id' => '2'
            ],
            [
                'feature_id'   => '2',
                'interface_id' => '3'
            ],
            [
                'feature_id'   => '3',
                'interface_id' => '1'
            ],
            [
                'feature_id'   => '4',
                'interface_id' => '1'
            ],
            [
                'feature_id'   => '4',
                'interface_id' => '2'
            ],
            [
                'feature_id'   => '5',
                'interface_id' => '1'
            ],
            [
                'feature_id'   => '5',
                'interface_id' => '2'
            ],
            [
                'feature_id'   => '5',
                'interface_id' => '3'
            ],
            [
                'feature_id'   => '6',
                'interface_id' => '1'
            ],
            [
                'feature_id'   => '6',
                'interface_id' => '2'
            ],
            [
                'feature_id'   => '6',
                'interface_id' => '3'
            ]
        ];

        foreach ($datas as $data) {
            DB::table('feature_interface')->insert($data);
        }
    }
}
